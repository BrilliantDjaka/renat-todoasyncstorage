import React from 'react';
import { StyleSheet, Text, View,TextInput,TouchableOpacity,ScrollView,AsyncStorage } from 'react-native';
import Note from './Component/note'
import {CheckBox} from 'react-native-elements'

export default class App extends React.Component {
 constructor(props){
   super(props);
   this.state={
     text:'',
     loaded:false,
     notes:[
      
     ]
   }
   if(!this.state.loaded){
    AsyncStorage.getItem('test',(error , result) => {
      if(result != null && result != undefined){
        this.setState({notes:JSON.parse(result)})
      }
    })
   }
 }

  changeHandler = (text) =>{
    this.setState({text:text})
  }

  addNote = ()=>{
   
    let states = this.state;
    if(this.state.text != ''){
    states.notes.push({text:this.state.text,checked:false});}
    else{
      alert("Insert Text")
      return;
    }
   
    this.setState(states);
    AsyncStorage.setItem('test',JSON.stringify(states.notes))
  }

  checkBoxHandler = (index) =>{
    const states = {...this.state}
    states.notes[index].checked = !this.state.notes[index].checked; 
    this.setState(states)
}
  removeComplete = () =>{
    let states = this.state
    if(states.notes == null){
      
      return;
    }
    else{
      for(let i = 0;i<states.notes.length;i++){
        
        if(states.notes[i].checked == true){
        
            states.notes.splice(i,1)
        }
      }
    }
    this.setState(states)
    AsyncStorage.setItem('test',JSON.stringify(states.notes))
   
  }

  
  render() {
    
    notes = () => this.state.notes == undefined ? null : this.state.notes.map((i,index)=>{
        return(
          
          <CheckBox onPress={()=>this.checkBoxHandler(index)} checked={this.state.notes[index].checked} key={index} title={i.text}  ></CheckBox>
        );
      }
    ) 
    
   
    
    return (
      <View style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent:'center',
        alignItems: 'stretch',
      }}>
        <View // Title
          style={{
            backgroundColor:'powderblue',
            height:50,
            
            justifyContent:'center'
          }}
        >
          <Text style={{
            color:"white",
            textAlign:'center'
          }}>To Do</Text>
        </View>
        <View //Input
          style={{
          marginHorizontal:6,
          height:50 ,
          flexDirection:'row',
          alignItems:'stretch',
          marginTop:10,}}>
            <TextInput 
            placeholder="Enter your text" 
            style={{flex:5,height:40}}
            onChangeText={(text)=>this.changeHandler(text)}
            value={this.state.text}
            />
            <TouchableOpacity 
            style={{flex:2,height:40,
                    backgroundColor:'skyblue',
                   
                    justifyContent:'center',
                    borderRadius:3,
                  }}
                    onPress={()=>this.addNote()}><Text style={{color:'white',textAlign:'center'}}>Add</Text></TouchableOpacity>
          </View>
        <ScrollView style={{
          
          flexDirection:'column', 
          marginTop:10,
          marginHorizontal:0,
        }}>
          {notes()}
        </ScrollView>

        <TouchableOpacity  style={{
          height:40,
          margin:6,
          borderRadius:6,
          borderColor:'skyblue',
          borderWidth:3,
          backgroundColor:'skyblue',
          
          }}
          onPress={()=> this.removeComplete()}>
          <Text style={{color:"white",textAlign:'center',alignSelf:'center',marginTop:4,}}>Remove Completed</Text> 
        </TouchableOpacity>
        
      </View>
    );
  }
}
